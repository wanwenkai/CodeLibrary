#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

int main()
{
	BIO *bio_cipher = NULL, *b = NULL;
	const EVP_CIPHER *c = EVP_aes_128_gcm();
//	const EVP_CIPHER *c = EVP_des_ecb();
	int len, i;
	char tmp[1024];
	unsigned char key[16], iv[16];

    OpenSSL_add_all_algorithms();

	/* encrypt */
	for (i = 0; i < 16; i++) {
		memset(&key[i], i+1, 1);
		memset(&iv[i], i+1, 1);
	}

	bio_cipher = BIO_new(BIO_f_cipher());
	BIO_set_cipher(bio_cipher, c, key, iv, 1);
	b = BIO_new(BIO_f_buffer());
	b = BIO_push(bio_cipher, b);
	len = BIO_write(bio_cipher, "openssl", 7);
	len = BIO_read(bio_cipher, tmp, 1024);
    printf("encrypt: %s\n", tmp);
	BIO_free(b);

	/* decrypt */
	BIO *bdec = NULL, *bd = NULL;
//	const EVP_CIPHER *cd = EVP_des_ecb();
	const EVP_CIPHER *cd = EVP_aes_128_gcm();

	bdec = BIO_new(BIO_f_cipher());
	BIO_set_cipher(bdec, cd, key, iv, 0);
	bd = BIO_new(BIO_f_buffer());
	bd = BIO_push(bdec, bd);
	len = BIO_write(bdec, tmp, len);
	len = BIO_read(bio_cipher, tmp, 1024);
	printf("decrypt: %s\n", tmp);
	BIO_free(bdec);

	return 0;
}
