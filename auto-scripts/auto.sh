#!/usr/bin/expect

#set run path
set fstack_run_path "/data/f-stack"
set qingyun_run_path "/home/wanwenkai/qingyun/apps"
set client_f_run_path "/home/wanwenkai/tlsGen"
set clienit_q_run_path "/home/wanwenkai/tlsGen"
set monitor_run_path "/home/wanwenkai/HCMonitor/HCMonitor"

#set ip
set server_ip 10.16.0.85
set client_ip 10.16.0.84
set monitor_ip 10.16.0.83

#set shell script
set fstack "./start_tls.sh"
set qingyun "./build/echo_server 2> err"
set client "./client"
set monitor "./start.sh"

#password
set password "ict6882#!"

#run f-stack
#function ssh_node()
#{
	spawn ssh root@$monitor_ip
	expect {
	"yes/no"
	{send "yes\r"; exp_continue;}
	"password:"
	{send "$password\r";}
	"Password:"
	{send "$password\r";}
	}
#}

#function print_info()
#{
	puts "\n************************************************"
	puts "\n   login $monitor_ip success"
	puts "\n************************************************"
#}


#function exce_shell()
#{
#	expect "root@*" {send "cd $1\r"}
#	expect "root@*" {send "sh $2\r"}
#	expect "root@*" {send "exit\r"}
#}

#ssh_node $monitor_ip
#print_info $monitor_ip
#exce_shell $fstack_run_path $fstack
