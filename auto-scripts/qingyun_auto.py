#!/bin/python

import os
import sys, getopt
import numpy as np


#s_list = ["10.16.0.85", "/home/wanwenkai/qingyun/apps/", "./build/echo_server -l 2000 2> err"]
#m_list = ["10.16.0.83", "/home/wanwenkai/HCMonitor/HCMonitor/", "./start.sh"]
#c_list = ["10.16.0.84", "/home/wanwenkai/tlsGen/", "./client"]

class node_init(object):
	def __init__(self, ip, code_dir, exce_sh):
		self.ip = ip
		self.code_dir = code_dir
		self.exce_sh = exce_sh

class arg_init(object):
	def __init__(self, ifile, ofile, fstack, qingyun):
		self.ifile = ifile
		self.ofile = ofile
		self.fstack = fstack
		self.qingyun = qingyun
	
	# one of fstack and qingyun must be set
	def arg_check(self):
		if self.fstack == 0 and self.qingyun == 0:
			print ("command error.")
			print("test.py -i <inputfile> -o <outputfile> -f <fstack> or -q <qingyun>")
	
def print_info(ilst, name):
	if len(ilst) < 6:
		print ("element less than 6.")

	ilst.sort()
	print (ilst[1:6])
	print ("", name, "99% avg delay = ", format(np.mean(ilst[1:6]), '.4g'))
	

def avg_99(init):
	# read raw file
	os.system('grep "0\.99 " ' + init.ifile + '>' + init.ofile)
	arr = np.loadtxt(init.ofile)
	ilst = arr[:,1]

	if init.fstack:
		print_info(ilst, "fstack")
	elif init.qingyun:
		print_info(arr[:,2], "qingyun high")
		print_info(arr[:,3], "qingyun low")


def arg_parse(argv, init):
	try:
		opts, args = getopt.getopt(argv, "hi:o:f:q:", ["ifile=", "ofile"])
	except getopt.GetoptError:
		print("test.py -i <inputfile> -o <outputfile> -f<fstack> -q <qingyun>")
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print("test.py -i <inputfile> -o <outputfile> -f<fstack> -q <qingyun>")
			sys.exit()
		elif opt in ("-i", "--ifile"):
			init.ifile = arg
		elif opt in ("-o", "--ofile"):
			init.ofile = arg
		elif opt in ("-f", "--fstack"):
			init.fstack = arg
		elif opt in ("-q", "--qingyun"):
			init.qingyun = arg


def main(argv):
	
#	s_node = node_init(s_list)
#	c_node = node_init(c_list)
#	m_node = node_init(m_list)

	init = arg_init('', '', 0, 0)
	arg_parse(argv, init)
	init.arg_check()
	print("输入文件为：", init.ifile)
	avg_99(init)

if __name__ == "__main__":
	main(sys.argv[1:])
